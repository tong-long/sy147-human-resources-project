import axios from "axios";

// 增 , 删 , 改 , 查

// 获取用户基本资料
export function getUserInfo(params) {
  return axios({
    url: "http://localhost:3001/data",
    method: "GET",
    params,
  });
}

// 获取用户基本资料
export function POSTUserInfo(data) {
  return axios({
    url: "http://localhost:3001/data",
    method: "POST",
    data,
  });
}

// 删除用户基本资料
export function DELETEUserInfo(id) {
  return axios({
    url: `http://localhost:3001/data/${id}`,
    method: "DELETE",
  });
}

// 获取用户详情资料
export function GETUserInfo(id) {
  return axios({
    url: `http://localhost:3001/data/${id}`,
  });
}

// 获取用户详情资料
export function PUTUserInfo(data) {
  return axios({
    url: `http://localhost:3001/data/${data.id}`,
    method: "PUT",
    data,
  });
}

// 搜索
export function GETsearch(name) {
  return axios({
    url: `http://localhost:3001/data/?name=${name}`,
    method: "GET",
  });
}
